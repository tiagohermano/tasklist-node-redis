var express = require('express')
var path = require('path')
var logger = require('morgan')
var bodyParser = require('body-parser')
var redis = require('redis')

// Instancia do express
var app = express();

// Criação de client
var client = redis.createClient()

// loga no console toda vez que o client é conectado
client.on('connect', function() {
  console.log('Server Redis Conectado')
})

// configuração do motor de views
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// Configuração dos plugins para o express
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

// Rotas do Express
// GET '/'
app.get('/', function(req, res) {
  client.lrange('tasks', 0, -1, function(err, tasks) {
    client.hgetall('call', function(err, call) {
      if(err) {
        console.log(err)
        return
      }
      
      // Renderizar o template com propriedades de título(title), tarefas(tasks) e compromisso(call)
      res.render('index', {
        title: 'Lista de Tarefas',
        tasks,
        call: call ? call : {
          name: "Apresentação de trabalho de Aplicações Distribuidas",
          company: "UFG",
          phone: "62 30897628",
          time: "Segunda-feira, 08 de Julho às 19:00"
        }
      })
    })
  })
})

// POST '/task/add' - Adicionar tarefas
app.post('/task/add', function(req, res) {
  const task = req.body.task

  // Inclui nova tarefa na estrutura de lista 'tasks'
  client.rpush('tasks', task, function(err, reply) {
    if(err) {
      console.log(err)
      return
    }
    console.log('Task Added')
    res.redirect('/')
  })
})

// POST '/task/delete' - Deletar tarefas
app.post('/task/delete', function(req, res) {
  const tasksToDel = req.body.tasks

  // Verifica se as tarefas selecionadas estão na estrutura de lista 'tasks' e as remove
  client.lrange('tasks', 0, -1, function(err, tasks) {
    for(let i=0; i<tasks.length; i++) {
      if(tasksToDel.indexOf(tasks[i]) > -1) {
        client.lrem('tasks', 0, tasks[i], function(err) {
          if(err) {
            console.log(err)
            return
          }
        })
      }
    }
    res.redirect('/')
  })
})

// POST '/call/add' - Adicionar Compromisso
app.post('/call/add', function(req, res) {
  const newCall = {}
  newCall.name = req.body.name;
  newCall.company = req.body.company;
  newCall.phone = req.body.phone;
  newCall.time = req.body.time;

  // Adicionar objeto 'newCall' à estrutura de dados 'call'
  client.hmset('call', ['name', newCall.name, 'company', newCall.company, 'phone', newCall.phone, 'time', newCall.time],
  function(err, reply) {
    if(err) {
      console.log(err)
      return
    }
    console.log(reply);
    res.redirect('/')
  })
})

// Inicar servidor
app.listen(3000)
console.log('Server iniciado na porta 3000...')

module.exports = app;