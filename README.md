## Lista de tarefas (Node.js + Redis)
Lista de tarefas e compromissos feita utilizando Redis como banco de dados não relacional. O Redis também pode ser usado como Cache para o BD ou Message-Broker.
O App foi feito utilizando as seguintes tecnologias(a maioria vimos em sala de aula):
* [Node.js](https://nodejs.org/en/) - Javascript Server-side
* [Express](https://expressjs.com/pt-br/) - Framework de Servers HTTP
* [Redis](https://redis.io/) - NOSQL BD, Cache, Message-Broker
* [EJS](https://ejs.co/) - Linguagem de template(Server-side rendering)

Arquivo `app.js` contém comentários que explicam seu conteúdo.

#### Requerimentos iniciais
* [Node.js](https://nodejs.org/en/)
* [Docker-Compose](https://docs.docker.com/compose/)

#### Setup
Instalar dependências:
```sh
npm install
```

Iniciar instância do Redis através do Docker-compose:
```sh
sudo docker-compose up
```

Executar projeto:
```sh
npm run start
```

[http://localhost:3000/](http://localhost:3000/)

#### Redis
Estruturas de dados utilizadas:
* List (Tarefas)
* Hash (Compromissos)

mais informações: [Estruturas de dados - Redis](https://redis.io/topics/data-types)

#### Referências
[Curso: Learn Redis from Scratch](https://www.eduonix.com/courses/Web-Development/learn-redis-from-scratch)

[Node Redis Client](http://redis.js.org/)

[Documentação Redis](https://redis.io/documentation)

[Tutorial de Redis](https://youtu.be/Hbt56gFj998)